---
description: v3.0.0 Release Notes
---

# v3.0 Release Notes

## v3.0.0b0 (2023-05-17)

### Enhancements

- [SD-646](https://ipfabric.atlassian.net/browse/SD-646) - Support NetBox 4.0.

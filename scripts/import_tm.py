import json

from ipfabric_netbox.models import IPFabricTransformMap
from ipfabric_netbox.utilities.transform_map import BuildTransformMaps


with open("transform_map.json", "r") as file:
    data = file.read()
IPFabricTransformMap.objects.all().delete()
BuildTransformMaps(json.loads(data))
